import React from 'react';
import ServicesItemPart from '../../componets/servicesItemPart/Index';
import Footer from '../../common/footer/Index';
import Header from '../../common/header/Index';
import ServicesPlansPart from '../../componets/servicesPart/ServicesPlansPart';
import { Link } from 'react-router-dom';

import {pages} from '../../../lang.json'
const {services}  = pages

class ServicesPage extends React.Component {

    render() { 
        return ( 
          <>
            <Header lang={this.props.lang} changeLang={this.props.changeLang}/>
            {
              /*====== PAGE TITLE PART START ======*/
            }
            <div className="page-title-area">
              <div className="container">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="page-title-item text-center">
                      <h2 className="title">{this.props.lang == 'English' ? services.title.en : services.title.ar}</h2>
                      <nav aria-label="breadcrumb">

                      
                        <ol className="breadcrumb">
                          <li className="breadcrumb-item">
                            <Link  to={"/"}>{this.props.lang == 'English' ? services.path.root.en : services.path.root.ar} </Link>
                          </li>
                          <li className="breadcrumb-item active" aria-current="page">
                          {this.props.lang == 'English' ? services.path.dest.en : services.path.dest.ar}
                          </li>
                        </ol>
                      </nav>
                    </div>
                    {/* page title */}
                  </div>
                </div>
                {/* row */}
              </div>
              {/* container */}
            </div>
            {
              /*====== PAGE TITLE PART ENDS ======*/
            }
            <ServicesItemPart lang={this.props.lang}/>
            <ServicesPlansPart lang={this.props.lang}/>
            <Footer lang={this.props.lang}/>
            </>
         );
    }
}
 
export default ServicesPage;