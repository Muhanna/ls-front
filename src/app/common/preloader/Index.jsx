import React from 'react';
import PagePreloader from './page';

export default class Preloader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            pageLoading: true,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                pageLoading: false,
            });
        }, 2000);
    }

    render() {
        return (
            <div className="preloader" style={this.state.pageLoading? {display: 'block'} : { display: 'none'}}>
                <PagePreloader/>
                <div className="loading">
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                    <div className="line" />
                </div>
            </div>
        );
    }
}
