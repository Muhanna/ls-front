import React from 'react';


class DataMiningPart extends React.Component {

    render() { 
        return ( 
          <div className="services-optimization-area pt-80 pb-120">
            <div className="container">
              <div className="row align-items-center">
                <div className="col-lg-6 order-lg-1 order-2">
                  <div className="services-optimization-thumb mt-40">
                    <img src="https://www.okta.com/sites/default/files/styles/product_deep_dive_hero_image/public/hero-image/SSO_Hero_Image-Desktop_Mobile.png" alt="" />
                  </div>
                  <div className="services-optimization-thumb-2">
                   
                  </div>
                </div>
                <div className="col-lg-6 order-lg-2 order-1">
                  <div className="services-optimization-content mt-40">
                    <span>The World Has Changed. Okta is Your Modern, Agile Single Sign-on Solution.</span>
                    <h3 className="title">Okta.</h3>
                    <p>
                      Secure single sign-on comes standard: Okta Verify One-Time Password (OTP) is now included for all SSO customers                                          
                    </p>
             
                  </div>
                </div>
              </div>
            </div>
          </div>
         );
    }
}
 
export default DataMiningPart;