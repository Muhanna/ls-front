import React from 'react';

import { whatWeDo } from "../../../lang.json";

const data = whatWeDo.item[2]

class ProcessDesignerPart extends React.Component {

    render() { 
        return ( 
          <div className="services-optimization-area pt-80 pb-120">
            <div className="container">
              <div className="row align-items-center">
                <div className="col-lg-6 order-lg-1 order-2">
                  <div className="services-optimization-thumb mt-40">
                    <img src="assets/images/qpr-3.png" alt="" />
                  </div>
                  <div className="services-optimization-thumb-2">
                   
                  </div>
                </div>
                <div className="col-lg-6 order-lg-2 order-1">
                  <div className="services-optimization-content mt-40">
                  {this.props.lang == 'English' ?
                    <span>{data.name.en}</span>
                    :
                    <span className="text-sm-right" style={{textAlign: 'right'}}>{data.name.ar}</span>
                    }
                    {this.props.lang == 'English' ?
                    <h3 className="title">{data.title.en}</h3>
                    :
                    <h3 className="title">{data.title.ar}</h3>
                  }
                  {this.props.lang == 'English' ?
                    <p>
                    {data.description.en}                  
                    </p>
                    :
                    <p className="text-sm-right" style={{textAlign: 'right'}}>
                    {data.description.ar}                  
                    </p>
                    } 
                  </div>
                </div>
              </div>
            </div>
          </div>
         );
    }
}
 
export default ProcessDesignerPart;