import React from 'react';
import Slider from "react-slick";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faLongArrowAltLeft,
    faLongArrowAltRight,
} from '@fortawesome/free-solid-svg-icons';

import { leaderShip } from '../../../lang.json'
import { Link } from 'react-router-dom';

function NextArrow(props) {
  const { style, onClick } = props;
  return (
    <span onClick={onClick} className="next slick-arrow" style={style}>
      <i className="fal fa-long-arrow-right" />
      <FontAwesomeIcon icon={faLongArrowAltRight}/>
    </span>
  );
}

function PrevArrow(props) {
  const { style, onClick } = props;
  return (
    <span onClick={onClick} className="prev slick-arrow" style={style}>
      <FontAwesomeIcon icon={faLongArrowAltLeft}/>
    </span>
  );

}

class LeadershipPart extends React.Component {

  render() { 
    var settings = {
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1201,
                settings: {
                    slidesToShow: 3,
                }
        },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                }
        },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                }
        }
      ]


    };

    return ( 
      <div className="leadership-area gray-bg pt-105 pb-160">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-10">
              <div className="section-title-2 text-center pl-25 pr-25">
                <h2 className="title">{this.props.lang == "English" ? leaderShip.name.en : leaderShip.name.ar}</h2>
                <p>
                {this.props.lang == "English" ? leaderShip.description.en : leaderShip.description.ar}
                </p>
              </div>
              {/* section title 2 */}
            </div>
          </div>
          {/* row */}
          <Slider className="row leadership-active" {...settings}>
            <div className="col-lg-4">
              <div className="leadership-item mt-30">
                <img src='assets/images/BaderAldoweesh.jpeg' alt="" />
                <div className="leadership-content">
                <Link  to={"Badr-Aldweesh"}> 
                  <h5 className="title">{this.props.lang == "English" ? leaderShip.member[0].name_en : leaderShip.member[0].name_ar}</h5>                  
                  <span>{this.props.lang == "English" ? leaderShip.member[0].en : leaderShip.member[0].ar}</span>
                  </Link>
                  </div>
              </div>
              {/* leadership item */}
            </div>
            <div className="col-lg-4">
              <div className="leadership-item mt-30">
                <img src="assets/images/Muhanna.jpeg" alt="" />
                <div className="leadership-content">
                  <h5 className="title">{this.props.lang == "English" ? leaderShip.member[1].name_en : leaderShip.member[1].name_ar}</h5>
                  <span>{this.props.lang == "English" ? leaderShip.member[1].en : leaderShip.member[1].ar}</span>
                </div>
              </div>
              {/* leadership item */}
            </div>
            <div className="col-lg-4">
              <div className="leadership-item mt-30">
                <img src="assets/images/Abdulkareem.jpeg" alt="" />
                <div className="leadership-content">
                  <h5 className="title">{this.props.lang == "English" ? leaderShip.member[2].name_en : leaderShip.member[2].name_ar}</h5>
                  <span>{this.props.lang == "English" ? leaderShip.member[2].en : leaderShip.member[2].ar}</span>
                </div>
              </div>
              {/* leadership item */}
            </div>
            
          </Slider>
          {/* row */}
        </div>
        {/* container */}
      </div>
      );
  }
}
 
export default LeadershipPart;