import React from 'react';

import {whatWeDo} from '../../../lang.json'

const data = whatWeDo.item[17];

const useStyles = makeStyles((theme) => ({
    item_container: {
      padding: '1rem'
    },
    item_sub_container: {
      backgroundColor: '#81a3bb',
      width: '100%',
      height: '10rem',
      padding: '1rem'
    },
    item_title: {
      fontSize: '1.7rem',
      color: '#000'
    },
    item_bar: {
      width: 8,
      height: '50%',
      position: 'relative',
      backgroundColor: '#fff',
      top: -56,
      left: -16
    },
    assesment_txt: {
      fontSize: '2.3rem',
      color: '#000',
      margin: '1rem'
    },
    online_txt: {
      fontSize: '3rem',
      color: '#000',
      margin: '1rem'
    },
    header_container: {
      width: '100%',
      justifyContent: 'center',
      display: 'flex',
      alignItems: 'center'
    },
    container: {
      marginTop: 200,
      marginBottom: 200
    }
  }));

class OnlineAssessmentPart extends React.Component{

    

}
