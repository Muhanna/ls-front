
/**
 * i18n.js
 * 
 * This contains react-intl initializaion
 * 
 * exports AppLocaleList, translatedMessages, DEFAULT_LOCALE
 */

const AppLocaleList = [
    { name: 'English', code: 'en', lang: 'English' },
    { name: 'Arabic', code: 'ar', lang: 'Arabic' }
];

const addLocaleData = require('react-intl').addLocaleData; 

const en = require('react-intl/locale-data/en');
// const ar = require('react-intl/locale-data/ar');


addLocaleData(en);
// addLocaleData(ar);


const enTranslationMessages = require('./assets/translations/en.json');
const arTranslationMessages = require('./assets/translations/ar.json');


const translatedMessages = {
    en: enTranslationMessages,
    ar: arTranslationMessages
};

const DEFAULT_LOCALE = navigator.language.match(/^[A-Za-z]{2}/)[0];

export { AppLocaleList, translatedMessages, DEFAULT_LOCALE };